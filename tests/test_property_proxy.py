# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from pytest import fixture

from goban.property_proxy import GameInfo
from goban.sgf.game_tree import SgfNode
from goban.sgf.properties import PROPS


@fixture
def node():
    return SgfNode(
        parent=None,
        properties={
            PROPS.HA: 3,
            PROPS.KM: 0.0,
            PROPS.RE: "B+Resign",
        }
    )


@fixture
def game_info(node):
    return GameInfo(node)


def test_read(game_info):
    assert game_info.komi == 0.0
    assert game_info.handicap == 3
    assert game_info.game_result == "B+Resign"
    assert game_info.game_name == ""


def test_gobject_read(game_info):
    assert game_info.props.komi == 0.0
    assert game_info.props.handicap == 3
    assert game_info.props.game_result == "B+Resign"
    assert game_info.props.game_name == ""


def test_write(node, game_info):
    game_info.black_name = "John Doe"
    game_info.white_name = "Jane Doe"
    game_info.komi = 0.5
    game_info.game_result = None

    assert node.properties.get(PROPS.PB) == "John Doe"
    assert node.properties.get(PROPS.PW) == "Jane Doe"
    assert node.properties.get(PROPS.KM) == 0.5
    assert node.properties.get(PROPS.RE) is None


def test_gobject_write(node, game_info):
    game_info.props.black_name = "John Doe"
    game_info.props.white_name = "Jane Doe"
    game_info.props.komi = 0.5
    game_info.props.game_result = None

    assert node.properties.get(PROPS.PB) == "John Doe"
    assert node.properties.get(PROPS.PW) == "Jane Doe"
    assert node.properties.get(PROPS.KM) == 0.5
    assert node.properties.get(PROPS.RE) is None
