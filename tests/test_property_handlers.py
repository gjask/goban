# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest.mock import Mock

from pytest import mark

from goban.game.model import Color, Move, Point
from goban.property_handlers import HANDLERS
from goban.sgf.properties import PROPS


@mark.parametrize(
    "props, expected_call",
    [
        ({PROPS.B: Point(3, 3)}, Move(Color.BLACK, Point(3, 3))),
        ({PROPS.W: Point(3, 4)}, Move(Color.WHITE, Point(3, 4))),
        ({}, None),

    ],
)
def test_move(props, expected_call):
    game_state = Mock(["board"])
    game_state.board = Mock(["play"])

    HANDLERS.process(game_state, props)

    if expected_call is None:
        assert game_state.board.play.call_count == 0
    else:
        game_state.board.play.assert_called_once_with(expected_call)


@mark.parametrize(
    "props, expected_call",
    [
        (
            {
                PROPS.AB: [Point(1, 1), Point(1, 2)],
            },
            {
                "black": [Point(1, 1), Point(1, 2)],
                "white": None,
                "empty": None,
            },
        ),
        (
            {
                PROPS.AW: [Point(1, 1), Point(1, 2)],
                PROPS.AE: [Point(2, 1)],
            },
            {
                "black": None,
                "white": [Point(1, 1), Point(1, 2)],
                "empty": [Point(2, 1)],
            },
        ),
        (
            {},
            None,
        ),
    ],
)
def test_setup_point(props, expected_call):
    game_state = Mock(["board"])
    game_state.board = Mock(["setup_stones"])

    HANDLERS.process(game_state, props)

    if expected_call is None:
        assert game_state.board.setup_stones.call_count == 0
    else:
        game_state.board.setup_stones.assert_called_once_with(**expected_call)


@mark.parametrize(
    "props, expected_call",
    [
        ({PROPS.PL: Color.WHITE}, Color.WHITE),
        ({PROPS.PL: Color.BLACK}, Color.BLACK),
        ({}, None),
    ]
)
def test_setup_player(props, expected_call):
    game_state = Mock(["board"])
    game_state.board = Mock(["setup_player"])

    HANDLERS.process(game_state, props)

    if expected_call is None:
        assert game_state.board.setup_player.call_count == 0
    else:
        game_state.board.setup_player.assert_called_once_with(expected_call)
