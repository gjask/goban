# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Optional

from pytest import mark

from goban.game.board import Board, Captures
from goban.game.model import Color, Move, Point

CHARS: dict[str, Optional[Color]] = {
    ".": None,
    "X": Color.BLACK,
    "O": Color.WHITE,
}


def split_diagram(double_diagram: list[str]) -> tuple[list[str], list[str]]:
    ldiagram, rdiagram = [], []
    for row in double_diagram:
        lrow, rrow = row.split("|", 1)
        ldiagram.append(lrow)
        rdiagram.append(rrow)
    return ldiagram, rdiagram


def parse_diagram(diagram: list[str]) -> dict[Point, Optional[Color]]:
    return {
        Point(row=y, column=x): CHARS[char]
        for y, row in enumerate(diagram)
        for x, char in enumerate(row)
    }


def board_from_diagram(diagram: list[str]) -> Board:
    board = Board(len(diagram))
    board._apply_color_patch(parse_diagram(diagram))
    return board


def assert_diagram(board: Board, diagram: list[str]):
    assert board.get_color_map() == parse_diagram(diagram)


def assert_captures(board: Board, black: int, white: int):
    assert board.captures == Captures(black, white)


def test_move_on_emtpy_board():
    board = Board(2)
    board.play(Move(Color.BLACK, Point(0, 0)))
    board.play(Move(Color.WHITE, Point(0, 1)))

    assert_diagram(board, [
        "XO",
        "..",
    ])


def test_color_patch():
    board = Board(3)
    board._apply_color_patch({
        Point(0, 1): Color.WHITE,
        Point(1, 1): Color.BLACK,
        Point(2, 2): Color.BLACK,
    })
    assert_diagram(board, [
        ".O.",
        ".X.",
        "..X",
    ])
    board._apply_color_patch({
        Point(0, 1): None,
        Point(1, 0): Color.BLACK,
        Point(2, 2): Color.WHITE,
    })
    assert_diagram(board, [
        "...",
        "XX.",
        "..O",
    ])


@mark.parametrize(
    "start_and_end, moves, black_capt, white_capt",
    [
        (  # stone capture
            [
                "X..|.O.",
                "O.X|O.X",
                "..O|.X.",
            ],
            [
                Move(Color.BLACK, Point(2, 1)),
                Move(Color.WHITE, Point(0, 1)),
            ],
            1, 1,
        ),
        (  # group capture
            [
                ".XO.|O.O.",
                "XXO.|..O.",
                "XO.O|.O.O",
                "OO..|OO..",
            ],
            [
                Move(Color.WHITE, Point(0, 0)),
            ],
            4, 0,
        ),
        (  # suicide
            [
                ".OX.|..X.",
                "OOX.|..X.",
                "XX.X|XX.X",
                "....|....",
            ],
            [
                Move(Color.WHITE, Point(0, 0)),
            ],
            0, 4,
        ),
        (  # playing over stone
            [
                "OOX.|..X.",
                "OX.X|XX.X",
                "O...|O...",
                "....|....",
            ],
            [
                Move(Color.BLACK, Point(1, 0)),
            ],
            0, 2,
        ),
        (  # ko
            [
                ".X..|.X..",
                "X.X.|X.X.",
                "OXO.|OXO.",
                ".O..|.O..",
            ],
            [
                Move(Color.WHITE, Point(1, 1)),
                Move(Color.BLACK, Point(2, 1)),
                Move(Color.WHITE, Point(1, 1)),
                Move(Color.BLACK, Point(2, 1)),
            ],
            2, 2,
        ),
    ],
    ids=[
        "stone capture",
        "group capture",
        "suicide",
        "playing over stone",
        "ko",
    ]
)
def test_moves(start_and_end, moves, black_capt, white_capt):
    start, end = split_diagram(start_and_end)
    board = board_from_diagram(start)

    for move in moves:
        board.play(move)

    assert_diagram(board, end)
    assert_captures(board, black_capt, white_capt)
