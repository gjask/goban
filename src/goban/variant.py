# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
from gi.repository import GLib

from goban.game.model import Point


def point_from_variant(variant: GLib.Variant) -> Point:
    return Point(
        row=variant.get_child_value(0).get_uint32(),
        column=variant.get_child_value(1).get_uint32(),
    )


def variant_from_point(point: Point) -> GLib.Variant:
    return GLib.Variant.new_tuple(
        GLib.Variant.new_uint32(point.row),
        GLib.Variant.new_uint32(point.column),
    )
