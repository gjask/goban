# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# ruff: noqa: E402


import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")
gi.require_version("Rsvg", "2.0")

from build_data import APP_ID, VERSION
from gi.repository import Adw, Gio

from goban.game_state import GameState
from goban.sgf.parser import parse_sgf
from goban.widgets.preferences_dialog import PreferencesDialog
from goban.widgets.window import GobanWindow


class GobanApplication(Adw.Application):
    """The main application singleton class."""

    settings: Gio.Settings

    def __init__(self):
        super().__init__(
            application_id=APP_ID,
            resource_base_path="/cz/skrle/Goban",
            flags=Gio.ApplicationFlags.HANDLES_OPEN,
        )
        self.settings = Gio.Settings(schema_id=APP_ID)

        self.create_action("quit", lambda *_: self.quit(), ["<primary>q"])
        self.create_action("about", self.on_about_action)
        self.create_action("preferences", self.on_preferences_action)

    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        window = self.get_active_window()
        if not window:
            window = GobanWindow(application=self)
        window.present()

    def do_open(self, files, count, hint):
        for file in files:
            file.load_contents_async(None, self.open_file_complete)
            self.hold()

    def open_file_complete(self, file, result):
        self.release()
        status, content, etag = file.load_contents_finish(result)

        if not status:  # fixme logging
            print(f"Unable to open {file.peek_path()}: {content}")
            return

        # fixme coding
        collection = parse_sgf(content.decode("utf-8"))
        game_state = GameState(collection)
        game_state.file = file

        window = self.get_active_window()

        if window and window.game_state.is_empty:
            window.game_state = game_state
        else:
            window = GobanWindow(application=self, game_state=game_state)

        window.present()

    def on_about_action(self, widget, _):
        """Callback for the app.about action."""
        dialog = Adw.AboutDialog.new_from_appdata(
            f"/cz/skrle/Goban/{APP_ID}.metainfo.xml",
            VERSION,
        )
        dialog.set_version(VERSION)
        dialog.set_designers(["Jakub Steiner"])
        dialog.set_developers(["Jan Škrle"])
        dialog.present(self.get_active_window())

    def on_preferences_action(self, widget, _):
        """Callback for the app.preferences action."""
        PreferencesDialog(self.settings).present(self.get_active_window())

    def create_action(self, name, callback, shortcuts=None, param_type=None):
        action = Gio.SimpleAction.new(name, param_type)
        action.connect("activate", callback)
        self.add_action(action)

        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)
