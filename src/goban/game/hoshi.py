# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from itertools import islice

from .model import Hoshi, Point


def generate_hoshi(board_size: int) -> list[Hoshi]:
    """
    Generates points for hoshi and potential handicap placements.
    """

    hoshi = []
    mid = board_size // 2

    def hs(row, col, skip_even=False):
        return Hoshi(
            Point(
                row=row % board_size,
                column=col % board_size,
            ),
            skip_even,
        )

    if board_size < 9:
        if board_size % 2 > 0:
            hoshi.append(hs(mid, mid, True))
    elif board_size < 13:
        hoshi.extend((hs(2, -3), hs(-3, 2), hs(-3, -3), hs(2, 2)))
    else:
        hoshi.extend((hs(3, -4), hs(-4, 3), hs(-4, -4), hs(3, 3)))
        if board_size % 2 > 0:
            hoshi.append(hs(mid, mid, True))
            if board_size >= 19:
                hoshi.extend(
                    (hs(mid, -4), hs(mid, 3), hs(3, mid), hs(-4, mid))
                )

    return hoshi


def default_handicap(board_size: int, handicaps: int) -> list[Point]:
    """
    Generates default handicap placements.
    """
    return list(
        islice(
            (
                hoshi.point
                for hoshi in generate_hoshi(board_size)
                if not hoshi.skip_even or handicaps % 2 == 1
            ),
            handicaps,
        )
    )
