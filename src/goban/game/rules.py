# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass


@dataclass
class Rules:
    permit_suicide: bool
    # todo
