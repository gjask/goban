# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from collections.abc import Iterable
from dataclasses import dataclass
from enum import Enum
from itertools import product
from typing import Self


@dataclass(frozen=True)
class Point:
    row: int
    column: int


@dataclass(frozen=True)
class Rectangle:
    start: Point
    end: Point

    def get_points(self) -> Iterable[Point]:
        return (
            Point(y, x)
            for y, x in product(
                range(self.start.row, self.end.row + 1),
                range(self.start.column, self.end.column + 1),
            )
        )


class Color(Enum):
    BLACK = 0
    WHITE = 1

    def inversion(self) -> Self:
        return self.__class__(self.value ^ 1)


@dataclass(frozen=True)
class Move:
    color: Color
    point: Point | None


@dataclass(frozen=True)
class Hoshi:
    point: Point
    skip_even: bool


@dataclass(frozen=True)
class Editor:
    name: str
    version: str
