# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from collections import deque
from collections.abc import Iterable
from dataclasses import dataclass
from itertools import product
from typing import Iterator, Optional, Self

from .hoshi import generate_hoshi
from .model import Color, Hoshi, Move, Point


@dataclass
class Captures:
    black: int = 0
    white: int = 0

    def add_captured(self, color: Color, stones: int):
        if color is Color.BLACK:
            self.black += stones
        else:
            self.white += stones


@dataclass(eq=False)
class StoneGroup:
    color: Color
    stones: frozenset[Point]
    liberties: set[Point]

    def merge(self, *others: Self) -> Self:
        if not others:
            return self

        return self.__class__(
            color=self.color,
            stones=self.stones.union(
                *(group.stones for group in others)
            ),
            liberties=self.liberties.union(
                *(group.liberties for group in others)
            ),
        )


class Board:
    board_size: int
    _stones: dict[Point, Optional[StoneGroup]]
    captures: Captures
    next_color: Color = Color.BLACK
    move_count: int
    hoshi: list[Hoshi]

    # todo rules object
    def __init__(self, board_size: int):
        self.board_size = board_size
        self._stones = self._get_empty_stones()
        self.captures = Captures()
        self.move_count = 0

        self.hoshi = generate_hoshi(board_size)

    def __getitem__(self, point: Point) -> Optional[Color]:
        group = self._stones[point]

        if group is not None:
            return group.color

    def _get_empty_stones(self) -> dict[Point, Optional[StoneGroup]]:
        return {
            Point(y, x): None
            for x, y in product(range(self.board_size), repeat=2)
        }

    def in_bounds(self, point: Point) -> bool:
        return (
            min(point.row, point.column) >= 0
            and max(point.row, point.column) < self.board_size
        )

    def get_neighbours(self, point: Point) -> Iterator[Point]:
        for x, y in ((-1, 0), (1, 0), (0, -1), (0, 1)):
            neighbour = Point(
                row=point.row + y,
                column=point.column + x,
            )

            if self.in_bounds(neighbour):
                yield neighbour

    def is_valid(self, point: Point) -> bool:
        if not self.in_bounds(point):
            raise ValueError(f"Invalid point: {point}")

        if self._stones[point] is not None:
            return False

        # todo
        return True

    def play(self, move: Move):
        if move.point is not None:
            self.place_stone(move.color, move.point)

        self.next_color = move.color.inversion()
        self.move_count += 1

    def place_stone(self, color: Color, point: Point):
        if not self.in_bounds(point):
            raise ValueError(f"Invalid point: {point}")

        if self._stones[point] is not None:
            self._apply_color_patch({point: None})

        liberties = set()
        friends = set()
        enemies = set()

        for neighbour_point in self.get_neighbours(point):
            neighbour_group = self._stones[neighbour_point]
            if neighbour_group is None:
                liberties.add(neighbour_point)
            elif neighbour_group.color is color:
                friends.add(neighbour_group)
            else:
                enemies.add(neighbour_group)

        group = StoneGroup(
            color=color,
            stones=frozenset((point, )),
            liberties=liberties,
        ).merge(*friends)
        group.liberties.discard(point)

        for pos in group.stones:
            self._stones[pos] = group

        for enemy in enemies:
            enemy.liberties.remove(point)

            if not enemy.liberties:
                self._remove_group(enemy)

        if not group.liberties:
            self._remove_group(group)

    def _remove_group(self, group: StoneGroup):
        for point in group.stones:
            for neighbour_point in self.get_neighbours(point):
                neighbour_group = self._stones[neighbour_point]
                if neighbour_group is None or neighbour_group is group:
                    continue
                neighbour_group.liberties.add(point)
            self._stones[point] = None
        self.captures.add_captured(group.color, len(group.stones))

    def setup_stones(
        self,
        black: Optional[Iterable[Point]] = None,
        white: Optional[Iterable[Point]] = None,
        empty: Optional[Iterable[Point]] = None,
    ):
        color_patch = {}

        for color, points in (
            (Color.BLACK, (black or ())),
            (Color.WHITE, (white or ())),
            (None, (empty or ())),
        ):
            for point in points:
                if not self.in_bounds(point):
                    raise ValueError(f"Invalid point {point}")

                color_patch[point] = color

        if color_patch:
            self._apply_color_patch(color_patch)

    def get_color_map(self) -> dict[Point, Optional[Color]]:
        return {
            point: None if group is None else group.color
            for point, group in self._stones.items()
        }

    def _apply_color_patch(self, color_patch: dict[Point, Optional[Color]]):
        color_map = self.get_color_map()
        color_map.update(color_patch)

        stones = self._get_empty_stones()
        processed = set()
        to_process = deque()

        for start_point, start_color in color_map.items():
            if start_color is None or start_point in processed:
                continue

            to_process.append(start_point)
            group_stones = set()
            liberties = set()

            while to_process:
                current_point = to_process.popleft()

                if current_point in processed:
                    continue

                current_color = color_map[current_point]

                if current_color is None:
                    liberties.add(current_point)

                elif current_color is start_color:
                    group_stones.add(current_point)
                    processed.add(current_point)
                    to_process.extend(self.get_neighbours(current_point))

            group = StoneGroup(
                color=start_color,
                stones=frozenset(group_stones),
                liberties=liberties,
            )

            for pos in group.stones:
                stones[pos] = group

        self._stones = stones

    def setup_player(self, next_color: Color):
        self.next_color = next_color
