# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass

from gi.repository import Gio, Gtk


def new_with_attrs(klass, **attrs):
    instance = klass()

    for key, value in attrs.items():
        setattr(instance, key, value)

    return instance


def new_with_props(klass, **props):
    instance = klass()

    for key, value in props.items():
        setattr(instance.props, key, value)

    return instance


@dataclass(frozen=True)
class RectangleSize:
    width: int
    height: int


class FileFilters:
    def __init__(self):
        self._list_store = Gio.ListStore.new(Gtk.FileFilter)

    def add_filter(self, name: str, pattern: str, mime_type: str):
        instance = Gtk.FileFilter.new()
        instance.set_name(name)
        instance.add_pattern(pattern)
        instance.add_mime_type(mime_type)
        self._list_store.append(instance)

    def install(self, file_dialog: Gtk.FileDialog):
        file_dialog.set_filters(self._list_store)


class Ruler:
    def __init__(self):
        self.sizes: dict[
            tuple[Gtk.Widget, Gtk.Orientation], tuple[int, int]
        ] = {}

    def get_measure(
        self, widget: Gtk.Widget, orientation: Gtk.Orientation
    ) -> tuple[int, int]:
        if (widget, orientation) not in self.sizes:
            if widget.should_layout:
                min, nat, _, _ = widget.measure(orientation, -1)
                self.sizes[widget, orientation] = min, nat
            else:
                self.sizes[widget, orientation] = 0, 0

        return self.sizes[widget, orientation]

    def get_minimal(
        self, widget: Gtk.Widget, orientation: Gtk.Orientation
    ) -> int:
        return self.get_measure(widget, orientation)[0]

    def get_natural(
        self, widget: Gtk.Widget, orientation: Gtk.Orientation
    ) -> int:
        return self.get_measure(widget, orientation)[1]
