# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject

from goban.sgf.game_tree import SgfNode
from goban.sgf.properties import PROPS, Property


class SgfProperty(GObject.Property):
    def __init__(self, sgf_property: Property, **kwargs):
        super().__init__(**kwargs)

        self.sgf_property = sgf_property

    def __get__(self, obj, obj_type):
        if obj is None:
            return self

        return obj._node.properties.get(
            self.sgf_property,
            self.sgf_property.value_type.default,
        )

    def __set__(self, obj, value):
        obj._node.set_property(self.sgf_property, value)
        obj.emit("updated")

    # maybe fix PyGObject?
    _default_setter = __set__


class PropertyProxy(GObject.Object):
    __gtype_name__ = "PropertyProxy"

    updated = GObject.Signal()

    def __init__(self, node: SgfNode, **kwargs):
        super().__init__()
        self._node = node

        for key, value in kwargs.items():
            if not hasattr(self, key):
                raise AttributeError(f"{self} has no attribute {key}")

            setattr(self, key, value)


class GameInfo(PropertyProxy):
    __gtype_name__ = "GameInfo"

    komi = SgfProperty(PROPS.KM, type=float)
    handicap = SgfProperty(PROPS.HA, type=int)
    black_name = SgfProperty(PROPS.PB, type=str)
    white_name = SgfProperty(PROPS.PW, type=str)
    black_rank = SgfProperty(PROPS.BR, type=str)
    white_rank = SgfProperty(PROPS.WR, type=str)
    game_name = SgfProperty(PROPS.GN, type=str)
    game_result = SgfProperty(PROPS.RE, type=str)


class RootInfo(PropertyProxy):
    __gtype_name__ = "RootInfo"

    file_format = SgfProperty(PROPS.FF, type=int)
    game_ident = SgfProperty(PROPS.GM, type=int)
    board_size = SgfProperty(PROPS.SZ, type=int)
