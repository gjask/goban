# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from enum import Enum

from gi.repository import Adw, Gtk


class CloseDialogResponse(Enum):
    CANCEL = "cancel"
    DISCARD = "discard"
    SAVE = "save"


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/close_dialog.ui")
class CloseDialog(Adw.AlertDialog):
    __gtype_name__ = "CloseDialog"
