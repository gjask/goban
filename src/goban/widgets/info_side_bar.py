# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, GObject, Gtk


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/info_side_bar.ui")
class InfoSideBar(Adw.Bin):
    __gtype_name__ = "InfoSideBar"

    game_name: Adw.EntryRow = Gtk.Template.Child()
    game_result: Adw.EntryRow = Gtk.Template.Child()
    black_name: Adw.EntryRow = Gtk.Template.Child()
    black_rank: Adw.EntryRow = Gtk.Template.Child()
    white_name: Adw.EntryRow = Gtk.Template.Child()
    white_rank: Adw.EntryRow = Gtk.Template.Child()

    def __init__(self):
        super().__init__()

        self.binding_group = GObject.BindingGroup.new()

        for prop_name in (
            "game_name",
            "game_result",
            "black_name",
            "black_rank",
            "white_name",
            "white_rank",
        ):
            self.binding_group.bind(
                prop_name,
                getattr(self, prop_name),
                "text",
                GObject.BindingFlags.BIDIRECTIONAL,
            )

    def on_game_info_change(self, game_state):
        self.binding_group.set_source(game_state.game_info)

    def connect_game_state(self, game_state):
        game_state.connect("notify::game-info", self.on_game_info_change)
        self.on_game_info_change(game_state)
