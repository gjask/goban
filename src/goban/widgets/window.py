# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from build_data import PROFILE, AppProfile
from gi.repository import Adw, Gio, GLib, GObject, Gtk

from goban.game_state import GameState
from goban.gobject import FileFilters
from goban.sgf.serializer import serialize_sgf
from goban.variant import point_from_variant
from goban.widgets.close_dialog import CloseDialog, CloseDialogResponse
from goban.widgets.goban import Goban
from goban.widgets.header_bar import HeaderBar
from goban.widgets.info_side_bar import InfoSideBar
from goban.widgets.move_navigation import MoveNavigation


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/window.ui")
class GobanWindow(Adw.ApplicationWindow):
    __gtype_name__ = "GobanWindow"

    split_view: Adw.OverlaySplitView = Gtk.Template.Child()
    header_bar: HeaderBar = Gtk.Template.Child()
    goban: Goban = Gtk.Template.Child()
    move_navigation: MoveNavigation = Gtk.Template.Child()
    info_side_bar: InfoSideBar = Gtk.Template.Child()

    _game_state: GameState

    @property
    def new_board_popover(self):
        return self.header_bar.new_board_popover

    @GObject.Property(type=GObject.Object)
    def game_state(self) -> GameState:
        return self._game_state

    @game_state.setter
    def game_state(self, value: GameState):
        self._game_state = value
        self._connect_game_state(value)
        self._game_state.emit("updated")

    def __init__(self, game_state=None, **kwargs):
        super().__init__(**kwargs)
        self._define_actions()
        self.connect("close-request", self.on_close)

        self._connect_settings(self.get_application().settings)

        if game_state is not None:
            self.game_state = game_state
        else:
            self.game_state = self.new_board_popover.build_game_state()

        if PROFILE == AppProfile.DEVELOPMENT:
            self.add_css_class("devel")

    def _define_actions(self):
        self.create_action("open", self.on_open, ["<primary>o"])
        self.create_action("new", self.on_new, ["<primary>n"])

        self.create_action(
            "close",
            lambda a, p: self.close(),
            ["<primary>w"],
        )
        self.create_action(
            "save",
            lambda a, p: self.save(),
            ["<primary>s"],
        )
        self.create_action(
            "save_as",
            lambda a, p: self.save(as_new=True),
            ["<primary><shift>s"],
        )
        self.create_action(
            "previous",
            lambda a, p: self.game_state.move_by(-1)
        )
        self.create_action(
            "next",
            lambda a, p: self.game_state.move_by(1)
        )
        self.create_action(
            "first",
            lambda a, p: self.game_state.move_to_start()
        )
        self.create_action(
            "last",
            lambda a, p: self.game_state.move_to_end()
        )
        self.create_action(
            "play",
            lambda a, p: self.game_state.play_next(point_from_variant(p)),
            param_type=GLib.VariantType.new("(uu)"),
        )
        self.create_action(
            "pass",
            lambda a, p: self.game_state.play_next(None),
        )
        self.create_action(
            "move_to_point",
            lambda a, p: self.game_state.move_to_point(point_from_variant(p)),
            param_type=GLib.VariantType.new("(uu)"),
        )
        self.create_action(
            "move_by",
            lambda a, p: self.game_state.move_by(p.get_int32()),
            param_type=GLib.VariantType.new("i"),
        )
        self.create_property_action(
            "show_info_sidebar",
            self.split_view,
            "show-sidebar",
        )

    def _connect_game_state(self, game_state):
        self.goban.connect_game_state(game_state)
        self.header_bar.connect_game_state(game_state)
        self.info_side_bar.connect_game_state(game_state)
        self.move_navigation.connect_game_state(game_state)

        game_state.connect("updated", self.on_updated)

    def _connect_settings(self, settings):
        self.goban.connect_settings(settings)

    def create_action(self, name, callback, shortcuts=None, param_type=None):
        action = Gio.SimpleAction.new(name, param_type)
        action.connect("activate", callback)
        self.add_action(action)

        if shortcuts:
            app = self.get_application()
            app.set_accels_for_action(f"win.{name}", shortcuts)

    def create_property_action(self, name, gobject, property):
        action = Gio.PropertyAction.new(name, gobject, property)
        self.add_action(action)

    def on_updated(self, game_state):
        if game_state.is_at_first:
            self.action_set_enabled("win.previous", False)
            self.action_set_enabled("win.first", False)
        else:
            self.action_set_enabled("win.previous", True)
            self.action_set_enabled("win.first", True)

        if game_state.is_at_last:
            self.action_set_enabled("win.next", False)
            self.action_set_enabled("win.last", False)
        else:
            self.action_set_enabled("win.next", True)
            self.action_set_enabled("win.last", True)

    def on_new(self, action, param):
        self.new_board_popover.popdown()

        if self.game_state.is_empty:
            self.game_state = self.new_board_popover.build_game_state()
        else:
            self.__class__(
                application=self.get_application(),
                game_state=self.new_board_popover.build_game_state(),
            ).present()

    def on_open(self, action, param):
        filters = FileFilters()
        filters.add_filter("SGF files", "*.sgf", "application/x-sgf")
        filters.add_filter("All files", "*", "application/octet-stream")

        dialog = Gtk.FileDialog()
        filters.install(dialog)

        dialog.open_multiple(
            parent=self,
            cancellable=None,
            callback=self.on_open_response,
        )

    def on_open_response(self, dialog, result):
        try:
            files = dialog.open_multiple_finish(result)
        except GLib.GError as err:
            if err.matches(Gtk.DialogError.quark(), Gtk.DialogError.DISMISSED):
                return
            raise

        self.get_application().open(files, "")

    def save(self, as_new=False, close_after=False):
        if as_new or not self.game_state.file:
            self.save_dialog(close_after=close_after)
        else:
            self.write_file(self.game_state.file, close_after=close_after)

    def save_dialog(self, close_after=False):
        filters = FileFilters()
        filters.add_filter("SGF file", "*.sgf", "application/x-sgf")
        filters.add_filter("Any file", "*", "application/octet-stream")

        dialog = Gtk.FileDialog()
        filters.install(dialog)

        dialog.save(
            parent=self,
            cancellable=None,
            callback=self.on_save_response,
            user_data=close_after,
        )

    def on_save_response(self, dialog, result, user_data):
        try:
            file = dialog.save_finish(result)
        except GLib.GError as err:
            if err.matches(Gtk.DialogError.quark(), Gtk.DialogError.DISMISSED):
                return
            raise

        self.write_file(file, close_after=user_data)

    def write_file(self, file, close_after=False):
        file.replace_contents_async(
            serialize_sgf(self.game_state.collection).encode(),
            etag=None,  # fixme ?
            make_backup=False,
            flags=Gio.FileCreateFlags.NONE,
            cancellable=None,
            callback=self.on_write_file_complete,
            user_data=close_after,
        )

    def on_write_file_complete(self, file, result, user_data):
        status, etag = file.replace_contents_finish(result)

        if not status:
            print(f"Unable to write {file.peek_path()}")
            return

        if user_data:
            self.destroy()

        self.game_state.file = file
        self.game_state.modified = False

    def on_close(self, instance):
        if self.game_state.modified:
            CloseDialog().choose(self, None, self.on_close_response)
            return True
        return False

    def on_close_response(self, dialog, result):
        choice = CloseDialogResponse(dialog.choose_finish(result))

        if choice is CloseDialogResponse.SAVE:
            self.save(close_after=True)
        elif choice is CloseDialogResponse.DISCARD:
            self.destroy()
