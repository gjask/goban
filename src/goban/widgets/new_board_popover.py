# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject, Gtk

from goban.game_state import GameState

BOARD_SIZES = (9, 13, 19)
MAX_HANDICAPS = {9: 4, 13: 5, 19: 9}


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/new_board_popover.ui")
class NewBoardPopover(Gtk.Popover):
    __gtype_name__ = "NewBoardPopover"

    board_toggle_box = Gtk.Template.Child()
    handi_spinbutton = Gtk.Template.Child()
    komi_spinbutton = Gtk.Template.Child()

    _board_toggles: dict[int, Gtk.ToggleButton]

    @GObject.Property(type=int)
    def board_size(self):
        for size, button in self._board_toggles.items():
            if button.get_active():
                return size

    @board_size.setter
    def board_size(self, value):
        button = self._board_toggles.get(value)

        if button is None:
            raise ValueError(f"Size {value} not in presets")

        button.set_active(True)

    @GObject.Property(type=int)
    def handicap(self):
        return self.handi_spinbutton.get_value_as_int()

    @handicap.setter
    def handicap(self, value):
        self.handi_spinbutton.set_value(value)

    @GObject.Property(type=float)
    def komi(self):
        return self.komi_spinbutton.get_value()

    @komi.setter
    def komi(self, value):
        self.komi_spinbutton.set_value(value)

    def __init__(self):
        super().__init__()
        self._populate_board_toggle_box()
        self._connect_private_signals()

        self.connect("notify::board-size", self.on_board_size)

    def on_board_size(self, widget, param):
        self.handi_spinbutton.set_range(0, MAX_HANDICAPS[self.board_size])

    def _connect_private_signals(self):
        self.handi_spinbutton.connect(
            "value-changed",
            lambda _: self.notify("handicap"),
        )
        self.komi_spinbutton.connect(
            "value-changed",
            lambda _: self.notify("komi"),
        )

    def _populate_board_toggle_box(self):
        self._board_toggles = {}
        group = None

        def on_board_toggled(button):
            if button.get_active():
                self.notify("board-size")

        for size in BOARD_SIZES:
            button = Gtk.ToggleButton.new_with_label(str(size))
            button.connect("toggled", on_board_toggled)
            self.board_toggle_box.append(button)
            self._board_toggles[size] = button

            if group is None:
                group = button
            else:
                button.set_group(group)

    def build_game_state(self):
        # fixme
        return GameState.new(
            board_size=self.board_size,
            handicap=self.handicap,
            komi=self.komi,
        )
