# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gtk


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/move_navigation.ui")
class MoveNavigation(Adw.Bin):
    __gtype_name__ = "MoveNavigation"

    move_counter = Gtk.Template.Child()

    def on_game_state_update(self, game_state):
        self.move_counter.set_label(f"{game_state.board.move_count}")

    def connect_game_state(self, game_state):
        game_state.connect("updated", self.on_game_state_update)
