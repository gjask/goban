# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gtk

from goban.widgets.new_board_popover import NewBoardPopover


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/header_bar.ui")
class HeaderBar(Adw.Bin):
    __gtype_name__ = "HeaderBar"

    new_board_popover: NewBoardPopover = Gtk.Template.Child()
    title: Adw.WindowTitle = Gtk.Template.Child()

    def on_game_info_update(self, *args):
        inf = self.game_state.game_info
        unnamed = False

        if inf.game_name:
            self.title.set_title(inf.game_name)
        elif inf.black_name and inf.white_name:
            self.title.set_title(
                f"{inf.black_name} [{inf.black_rank or "?"}] vs. "
                f"{inf.white_name} [{inf.white_rank or "?"}]"
            )
        elif self.game_state.file:
            self.title.set_title(self.game_state.file.get_basename())
            unnamed = True

        if self.game_state.file and not unnamed:
            self.title.set_subtitle(self.game_state.file.get_basename())
        else:
            self.title.set_subtitle("")

    def connect_game_state(self, game_state):
        self.game_state = game_state

        game_state.game_info.connect("updated", self.on_game_info_update)
        game_state.connect("notify::file", self.on_game_info_update)
        game_state.connect("notify::game-info", self.on_game_info_update)

        self.on_game_info_update(game_state)
