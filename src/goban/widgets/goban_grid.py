# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Iterator

from gi.repository import Gdk, GObject, Gtk

from goban.game.model import Color, Point
from goban.gobject import RectangleSize, new_with_attrs
from goban.textures import TextureStorage, TextureType
from goban.variant import variant_from_point

MINIMUM_SPOT_SIZE = 20


class GobanSpot(Gtk.Widget):
    __gtype_name__ = "GobanSpot"

    point: Point
    stone: Color | None = None

    _textures: TextureStorage
    _grid_texture: TextureType
    _hover: bool = False

    def __init__(self, point, textures, grid_texture):
        super().__init__()
        self.setup_input()
        self.point = point
        self._textures = textures
        self._grid_texture = grid_texture

    def setup_input(self):
        motion = Gtk.EventControllerMotion()
        motion.connect("enter", self.on_enter)
        motion.connect("leave", self.on_leave)
        self.add_controller(motion)

    def on_enter(self, controller, x, y):
        # fixme is_valid
        self._hover = True
        self.queue_draw()

    def on_leave(self, controller):
        self._hover = False
        self.queue_draw()

    # fixme maybe?
    def _get_stone_texture(self):
        if self.stone is None:
            return None
        if self.stone is Color.BLACK:
            return TextureType.BSTONE
        return TextureType.WSTONE

    # fixme
    def _get_hover_texture(self):
        if not self._hover or self.stone is not None:
            return None
        if self.get_parent()._next_color is Color.BLACK:
            return TextureType.SBSTONE
        return TextureType.SWSTONE

    def do_snapshot(self, snapshot):
        size = RectangleSize(
            width=self.get_width(),
            height=self.get_height(),
        )

        for texture_type in (
            self._grid_texture,
            self._get_stone_texture(),
            self._get_hover_texture(),
        ):
            if texture_type is None:
                continue

            texture = self._textures.get_texture(texture_type, size)
            texture.snapshot(
                snapshot=snapshot,
                width=size.width,
                height=size.height,
            )


class GobanGridLayout(Gtk.LayoutManager):
    __gtype_name__ = "GobanGridLayout"

    def do_measure(self, widget, orientation, for_size):
        return (
            MINIMUM_SPOT_SIZE * widget._board_size,
            MINIMUM_SPOT_SIZE * widget._board_size,
            -1,
            -1,
        )

    def do_allocate(self, widget, width, height, baseline):
        side = min(width, height)
        spot_size = side // widget._board_size
        corrected_side = spot_size * widget._board_size

        left_offset = (width - corrected_side) // 2
        top_offset = (height - corrected_side) // 2

        for child in widget:
            if not child.should_layout():
                continue

            child.size_allocate(
                allocation=new_with_attrs(
                    Gdk.Rectangle,
                    x=child.point.column * spot_size + left_offset,
                    y=child.point.row * spot_size + top_offset,
                    width=spot_size,
                    height=spot_size,
                ),
                baseline=-1,
            )


class GobanGrid(Gtk.Widget):
    __gtype_name__ = "GobanGrid"

    _board_size: int = -1
    _next_color: Color = Color.BLACK
    _spots: dict[Point, GobanSpot]

    textures: TextureStorage = GObject.Property(type=GObject.Object)

    def __init__(self) -> None:
        super().__init__()
        self.setup_input()

        # fixme (also use settings)
        self.textures = TextureStorage.new_from_gresource(
            "/cz/skrle/Goban/themes/simple.svg"
        )

    def setup_input(self):
        click = Gtk.GestureClick()
        click.connect("released", self.on_button_release)
        self.add_controller(click)

    def on_button_release(self, controller, clicks, x, y):
        if not self.contains(x, y):
            return

        spot = self.pick(x, y, Gtk.PickFlags.DEFAULT)

        if not spot:
            return

        if (
            controller.get_current_event_state()
            & Gdk.ModifierType.CONTROL_MASK
        ):
            self.activate_action(
                "win.move_to_point",
                variant_from_point(spot.point),
            )

        elif spot.stone is None:
            self.activate_action(
                "win.play",
                variant_from_point(spot.point)
            )

    def repopulate(self, game_state):
        self._board_size = game_state.board.board_size
        self._spots = {}

        for child in list(self):
            child.unparent()

        for point, texture_type in grid_generator(
            game_state.board.board_size, game_state.board.hoshi
        ):
            spot = GobanSpot(
                point=point,
                textures=self.textures,
                grid_texture=texture_type,
            )

            spot.set_parent(self)
            self._spots[point] = spot

    def update(self, game_state):
        if game_state.board.board_size != self._board_size:
            self.repopulate(game_state)

        for point, stone in game_state.board.get_color_map().items():
            spot = self._spots[point]
            if spot.stone is not stone:
                spot.stone = stone
                spot.queue_draw()

        self._next_color = game_state.board.next_color

    # fixme
    def do_unroot(self):
        for child in list(self):
            child.unparent()


def grid_generator(
    board_size: int, hoshi: list[Point]
) -> Iterator[tuple[Point, TextureType]]:
    if board_size < 2:
        raise ValueError("Can not generate grid smaller than 2x2.")

    hoshi = {h.point for h in hoshi}
    max_xy = board_size - 1

    yield (Point(0, 0), TextureType.TOP_LEFT)
    yield from ((Point(0, x), TextureType.TOP) for x in range(1, max_xy))
    yield (Point(0, max_xy), TextureType.TOP_RIGHT)

    for y in range(1, max_xy):
        yield (Point(y, 0), TextureType.LEFT)

        for x in range(1, max_xy):
            pos = Point(y, x)
            tex = TextureType.HOSHI if pos in hoshi else TextureType.MIDDLE
            yield (pos, tex)

        yield (Point(y, max_xy), TextureType.RIGHT)

    yield (Point(max_xy, 0), TextureType.BOTTOM_LEFT)
    yield from (
        (Point(max_xy, x), TextureType.BOTTOM) for x in range(1, max_xy)
    )
    yield (Point(max_xy, max_xy), TextureType.BOTTOM_RIGHT)
