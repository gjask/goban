# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from string import ascii_uppercase

from gi.repository import Gdk, Gio, GLib, GObject, Gtk

from goban.gobject import Ruler, new_with_attrs
from goban.widgets.goban_grid import GobanGrid

_HORIZONTAL = Gtk.Orientation.HORIZONTAL
_VERTICAL = Gtk.Orientation.VERTICAL


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/goban.ui")
class Goban(Gtk.Widget):
    __gtype_name__ = "Goban"

    grid: GobanGrid = Gtk.Template.Child()
    top_axis: Gtk.Box = Gtk.Template.Child()
    left_axis: Gtk.Box = Gtk.Template.Child()
    right_axis: Gtk.Box = Gtk.Template.Child()
    bottom_axis: Gtk.Box = Gtk.Template.Child()

    spacing: int = GObject.Property(type=int)

    _board_size: int = -1

    def __init__(self):
        super().__init__()
        self.setup_input()

    def setup_input(self):
        scroll = Gtk.EventControllerScroll()
        scroll.set_flags(
            Gtk.EventControllerScrollFlags.VERTICAL
            | Gtk.EventControllerScrollFlags.DISCRETE
        )
        scroll.connect("scroll", self.on_scroll)
        self.add_controller(scroll)

    def connect_game_state(self, game_state):
        game_state.connect("updated", self.on_game_state_update)

    def connect_settings(self, settings):
        for axis in (
            self.top_axis,
            self.left_axis,
            self.right_axis,
            self.bottom_axis,
        ):
            settings.bind(
                "display-coordinates",
                axis,
                "visible",
                Gio.SettingsBindFlags.GET,
            )

    def on_scroll(self, controller, dx, dy):
        self.activate_action("win.move_by", GLib.Variant.new_int32(int(dy)))

    def on_game_state_update(self, game_state):
        self.grid.update(game_state)

        if self._board_size != game_state.board.board_size:
            self.repopulate_axes(game_state.board.board_size)

    def repopulate_axes(self, board_size: int):
        letters = [c for c in ascii_uppercase if c != "I"][:board_size]
        numbers = [str(i) for i in range(board_size, 0, -1)]

        for axis, legend in (
            (self.top_axis, letters),
            (self.bottom_axis, letters),
            (self.left_axis, numbers),
            (self.right_axis, numbers),
        ):
            for child in list(axis):
                child.unparent()

            for name in legend:
                label = Gtk.Inscription.new(name)
                label.set_xalign(0.5)
                label.set_yalign(0.5)
                label.set_hexpand(True)
                label.set_vexpand(True)
                axis.append(label)


class GobanLayout(Gtk.LayoutManager):
    __gtype_name__ = "GobanLayout"

    def do_measure(self, widget, orientation, for_size):
        ruler = Ruler()

        if orientation is _HORIZONTAL:
            return (
                self._horizontal_measure(widget, ruler.get_minimal),
                self._horizontal_measure(widget, ruler.get_natural),
                -1,
                -1,
            )
        else:
            return (
                self._vertical_measure(widget, ruler.get_minimal),
                self._vertical_measure(widget, ruler.get_natural),
                -1,
                -1,
            )

    def _horizontal_measure(self, widget, getter):
        side = max(
            getter(widget.top_axis, _HORIZONTAL),
            getter(widget.bottom_axis, _HORIZONTAL),
            getter(widget.grid, _HORIZONTAL),
            getter(widget.left_axis, _VERTICAL),
            getter(widget.right_axis, _VERTICAL),
        )

        if over := side % widget._board_size:
            side += widget._board_size - over

        return (
            side
            + getter(widget.right_axis, _HORIZONTAL)
            + getter(widget.left_axis, _HORIZONTAL)
        )

    def _vertical_measure(self, widget, getter):
        side = max(
            getter(widget.left_axis, _VERTICAL),
            getter(widget.right_axis, _VERTICAL),
            getter(widget.grid, _VERTICAL),
            getter(widget.top_axis, _HORIZONTAL),
            getter(widget.bottom_axis, _HORIZONTAL),
        )

        if over := side % widget._board_size:
            side += widget._board_size - over

        return (
            side
            + getter(widget.top_axis, _VERTICAL)
            + getter(widget.bottom_axis, _VERTICAL)
        )

    def do_allocate(self, widget, width, height, baseline):
        ruler = Ruler()

        side = (
            min((
                width
                - ruler.get_natural(widget.left_axis, _HORIZONTAL)
                - ruler.get_natural(widget.right_axis, _HORIZONTAL)
            ), (
                height
                - ruler.get_natural(widget.top_axis, _VERTICAL)
                - ruler.get_natural(widget.bottom_axis, _VERTICAL)
            ))
            // widget.grid._board_size
            * widget.grid._board_size
        )

        top = (height - side) // 2
        left = (width - side) // 2

        if widget.grid.should_layout():
            widget.grid.size_allocate(
                allocation=new_with_attrs(
                    Gdk.Rectangle,
                    x=left,
                    y=top,
                    width=side,
                    height=side,
                ),
                baseline=-1,
            )

        for child, orientation, start in (
            (widget.top_axis, _VERTICAL, True),
            (widget.bottom_axis, _VERTICAL, False),
            (widget.left_axis, _HORIZONTAL, True),
            (widget.right_axis, _HORIZONTAL, False),
        ):
            if not child.should_layout():
                continue

            child_size = ruler.get_natural(child, orientation)

            if start:
                offset = - child_size
            else:
                offset = side

            if orientation is _HORIZONTAL:
                allocation = new_with_attrs(
                    Gdk.Rectangle,
                    x=left + offset,
                    y=top,
                    width=child_size,
                    height=side,
                )
            else:
                allocation = new_with_attrs(
                    Gdk.Rectangle,
                    x=left,
                    y=top + offset,
                    width=side,
                    height=child_size,
                )

            child.size_allocate(allocation, -1)
