# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gio, Gtk


@Gtk.Template(resource_path="/cz/skrle/Goban/ui/preferences_dialog.ui")
class PreferencesDialog(Adw.PreferencesDialog):
    __gtype_name__ = "PreferencesDialog"

    display_coordinates = Gtk.Template.Child()

    def __init__(self, settings):
        super().__init__()

        settings.bind(
            "display-coordinates",
            self.display_coordinates,
            "active",
            Gio.SettingsBindFlags.DEFAULT,
        )
