# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass
from typing import Any, Callable, Optional

from goban.game.model import Color, Move
from goban.sgf.properties import PROPS, Property


@dataclass
class HandlerData:
    callback: Callable
    data: dict[str, Any]
    group: Optional[set[Property]] = None


# todo tests
class HandlerRegistry:
    def __init__(self):
        self._handlers: dict[Property, HandlerData] = {}

    def register(self, property: Property, **data: Any):
        def decorator(func: Callable):
            self._handlers[property] = HandlerData(
                callback = func,
                data = data,
            )
            return func
        return decorator

    def register_group(self, *properties: Property, **data: Any):
        def decorator(func: Callable):
            self._handlers.update({
                prop: HandlerData(
                    callback = func,
                    data = data,
                    group = set(properties),
                ) for prop in properties
            })
            return func
        return decorator

    def process(self, game_state, properties: dict[Property, Any]):
        processed = set()

        for property, value in properties.items():
            handler = self._handlers.get(property)

            if handler is None:
                continue

            if handler.group is None:
                handler.callback(
                    game_state = game_state,
                    property = property,
                    value = value,
                    **handler.data,
                )

            elif property not in processed:
                handler.callback(
                    game_state = game_state,
                    properties = {
                        prop: properties.get(prop) for prop in handler.group
                    },
                    **handler.data,
                )
                processed.update(handler.group)


HANDLERS = HandlerRegistry()


@HANDLERS.register(PROPS.B, color=Color.BLACK)
@HANDLERS.register(PROPS.W, color=Color.WHITE)
def move(game_state, property, value, color):
    game_state.board.play(Move(color, value))


@HANDLERS.register_group(PROPS.AB, PROPS.AW, PROPS.AE)
def setup_point(game_state, properties):
    game_state.board.setup_stones(
        black = properties[PROPS.AB],
        white = properties[PROPS.AW],
        empty = properties[PROPS.AE],
    )


@HANDLERS.register(PROPS.PL)
def setup_player(game_state, property, value):
    game_state.board.setup_player(value)
