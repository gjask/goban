# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from enum import Enum
from typing import Self

from cairo import Context, Format, ImageSurface
from gi.repository import Gdk, GdkPixbuf, Gio, GObject, Rsvg

from goban.gobject import RectangleSize, new_with_attrs

DEFAULT_TEXTURE_SIZE = 64


class TextureType(Enum):
    BSTONE = "#bstone"
    WSTONE = "#wstone"
    SBSTONE = "#sbstone"
    SWSTONE = "#swstone"
    TOP_LEFT = "#topleft"
    TOP = "#top"
    TOP_RIGHT = "#topright"
    LEFT = "#left"
    MIDDLE = "#middle"
    HOSHI = "#hoshi"
    RIGHT = "#right"
    BOTTOM_LEFT = "#bottomleft"
    BOTTOM = "#bottom"
    BOTTOM_RIGHT = "#bottomright"


def render_element(
    handle: Rsvg.Handle,
    element_id: str,
    size: RectangleSize,
) -> GdkPixbuf.Pixbuf:
    _, ink_rect, _ = handle.get_geometry_for_element(element_id)

    with ImageSurface(Format.ARGB32, size.width, size.height) as surface:
        context = Context(surface)

        context.scale(
            size.width / ink_rect.width,
            size.height / ink_rect.height,
        )

        viewport = new_with_attrs(
            Rsvg.Rectangle,
            x=0,
            y=0,
            width=ink_rect.width,
            height=ink_rect.height,
        )

        handle.render_element(context, element_id, viewport)

        return Gdk.pixbuf_get_from_surface(
            surface=surface,
            src_x=0,
            src_y=0,
            width=size.width,
            height=size.height,
        )


class TextureStorage(GObject.Object):
    __gtype_name__ = "TextureStorage"

    _rsvg_handle: Rsvg.Handle
    _textures: dict[TextureType, Gdk.Texture]
    _texture_size: RectangleSize

    @classmethod
    def new_from_gresource(
        cls,
        gresource_uri: str,
        size: RectangleSize = None,
    ) -> Self:
        return cls(
            Rsvg.Handle.new_from_gfile_sync(
                file=Gio.File.new_for_uri(f"resource://{gresource_uri}"),
                flags=Rsvg.HandleFlags.FLAGS_NONE,
                cancellable=None,
            ),
            size=size,
        )

    def __init__(
        self,
        rsvg_handle: Rsvg.Handle,
        size: RectangleSize = None,
    ) -> None:
        super().__init__()

        self._rsvg_handle = rsvg_handle
        self._render_all_textures(
            size or RectangleSize(DEFAULT_TEXTURE_SIZE, DEFAULT_TEXTURE_SIZE),
        )

    def _render_all_textures(self, size: RectangleSize):
        self._texture_size = size
        self._textures = {
            texture_type: Gdk.Texture.new_for_pixbuf(
                render_element(
                    handle=self._rsvg_handle,
                    element_id=texture_type.value,
                    size=size,
                )
            )
            for texture_type in TextureType
        }

    def get_texture(
        self,
        texture_type: TextureType,
        size: RectangleSize,
    ) -> Gdk.Texture:
        if size != self._texture_size:
            self._render_all_textures(size)
        return self._textures[texture_type]
