# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass
from typing import Any, Optional

from tatsu import compile as compile_grammar

from goban.sgf.converters import Context, size_from_sgf
from goban.sgf.game_tree import SgfCollection, SgfGameTree, SgfNode
from goban.sgf.properties import PROPS, Composition, Property

sgf_grammar = compile_grammar(r"""
    start       = collection $ ;
    collection  = { new_root heads+:game_tree }+ ;
    new_root    = () ;
    game_tree   = "(" head:sequence { branches+:game_tree }* ")" ;
    sequence    = { nodes+:node }+ ;
    node        = n:";" { props+:property }* ;
    property    = ident:prop_ident { vals+:prop_value }+ ;
    prop_ident  = /[A-Z]+/ ;
    prop_value  = LB val:VALUE RB ;
    LB          = "[" ;
    VALUE        = /(?:[^\]\\]+|\\.)*/ ;
    RB          = "]" ;
""")


@dataclass(frozen=True)
class RawProperty:
    """
    Container to pack Property together with unparsed raw value.
    """
    prop_info: Property
    raw_value: list[str]

    def parse_value(self, context: Context) -> Any:
        """
        Converts raw value into correct python representation
        of property value.
        """
        if self.prop_info.value_type.composition is Composition.SINGLE:
            return self.prop_info.value_type.load(context, self.raw_value[0])

        return [
            self.prop_info.value_type.load(context, val)
            for val in self.raw_value
        ]


class SgfSemantics:
    """
    Contains semantic actions for corresponding grammar rules.
    https://tatsu.readthedocs.io/en/stable/semantics.html
    """

    def __init__(self):
        self.context: Optional[Context] = None

    def collection(self, ast) -> SgfCollection:
        """
        Instantiates `SgfCollection`.
        """
        return SgfCollection(
            [SgfGameTree(root) for root in ast.heads]
        )

    def new_root(self, ast):
        """
        Reset Context before new top level game tree.
        """
        self.context = None

    def game_tree(self, ast) -> SgfNode:
        """
        Links heads of sub-trees to tail of sequence. Returns head node.
        """
        if ast.branches:
            tail = ast.head

            while tail.children:
                tail = tail.children[0]

            for child in ast.branches:
                tail.children.append(child)
                child.parent = tail

        return ast.head

    def sequence(self, ast) -> SgfNode:
        """
        Links nodes in sequence. Returns head node.
        """
        head = prev = ast.nodes[0]

        for current in ast.nodes[1:]:
            prev.children.append(current)
            current.parent = prev
            prev = current

        return head

    def node(self, ast) -> SgfNode:
        """
        Sets new `Context` (only for a root node in each game tree).
        Parses all property values. And instantiates single `SgfNode`
        """
        # first parsed node in top level game tree is root node
        if self.context is None:
            props = {
                raw_prop.prop_info: raw_prop.raw_value
                for raw_prop in ast.props
            }
            self.context = Context(
                board_size=size_from_sgf(None, props[PROPS.SZ][0]),
                encoding=props.get(PROPS.CA, (None, ))[0],
            )

        # maybe validate prop types?
        return SgfNode(
            parent = None,
            properties = {
                raw_prop.prop_info: raw_prop.parse_value(self.context)
                for raw_prop in ast.props
            }
        )

    def property(self, ast) -> RawProperty:
        """
        Packs `Property` together with unparsed value into `RawProperty`.
        """
        return RawProperty(
            prop_info = PROPS.get_property(ast.ident),
            raw_value = ast.vals,
        )

    def prop_value(self, ast) -> str:
        """
        Passes unparsed property value.
        """
        return ast.val


def parse_sgf(text: str) -> SgfCollection:
    """
    Main parsing function.
    """
    return sgf_grammar.parse(text, semantics=SgfSemantics())
