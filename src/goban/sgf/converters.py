# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass
from re import compile as re_compile
from string import ascii_letters
from typing import Optional

from goban.game.model import Color, Editor, Point

# escape closing bracket `]`, escape char `\` and delimiter `:`
ESCAPE_RE = re_compile(r"[\]\\:]")

# un-escape any char after escape char
UNESCAPE_RE = re_compile(r"\\(.)")

# split composed properties only by not escaped delimiter
SPLIT_RE = re_compile(r"(?<!\\):")

# delimiter for joining composed properties
DELIMITER = ":"

# legacy pass value for 19x19 board
LEGACY_PASS = "tt"  # noqa: S105


@dataclass
class Context:
    board_size: int
    encoding: Optional[str]


def point_from_sgf(context: Context, sgf_point: str) -> Optional[Point]:
    if (
        not sgf_point
        or (sgf_point == LEGACY_PASS and context.board_size == 19)
    ):
        return None

    col, row = sgf_point
    return Point(
        column=ascii_letters.index(col),
        row=ascii_letters.index(row),
    )


def sgf_from_point(context: Context, point: Optional[Point]) -> str:
    if point is None:
        return ""
    return ascii_letters[point.column] + ascii_letters[point.row]


def color_from_sgf(context: Context, sgf_color: str) -> Color:
    return {
        "B": Color.BLACK,
        "W": Color.WHITE,
    }[sgf_color]


def sgf_from_color(context: Context, color: Color) -> str:
    return "B" if color is Color.BLACK else "W"


def size_from_sgf(context: Optional[Context], sgf_size: str) -> int:
    # todo rectangular board sizes
    return int(sgf_size)


def sgf_from_size(context: Context, size: int) -> str:
    return str(size)


def text_from_sgf(context: Context, sgf_text: str) -> str:
    return UNESCAPE_RE.sub(r"\g<1>", sgf_text)


def sgf_from_text(context: Context, text: str) -> str:
    return ESCAPE_RE.sub(r"\\\g<0>", text)


def editor_from_sgf(context: Context, sgf_editor: str) -> Editor:
    name, version = SPLIT_RE.split(sgf_editor, maxsplit=1)
    return Editor(
        # fixme simpletext
        name=text_from_sgf(context, name),
        version=text_from_sgf(context, version),
    )


def sgf_from_editor(context: Context, editor: Editor) -> str:
    return DELIMITER.join((
        # fixme simpletext
        sgf_from_text(context, editor.name),
        sgf_from_text(context, editor.version),
    ))
