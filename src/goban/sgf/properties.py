# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass
from enum import Enum, auto
from typing import Any, Callable, Optional

from goban.sgf.converters import (
    color_from_sgf,
    editor_from_sgf,
    point_from_sgf,
    sgf_from_color,
    sgf_from_editor,
    sgf_from_point,
    sgf_from_size,
    sgf_from_text,
    size_from_sgf,
    text_from_sgf,
)


class PropType(Enum):
    """
    SGF applies rules to which property types can coexist in single node.
    https://www.red-bean.com/sgf/sgf4.html#2.2.1
    """
    NONE = auto()
    MOVE = auto()
    SETUP = auto()
    ROOT = auto()
    GAME_INFO = auto()


class Composition(Enum):
    """
    Defines if property contains single, many or any number of values.
    """
    SINGLE = auto()
    LIST = auto()
    ELIST = auto()


@dataclass(frozen=True)
class ValueTypeInfo:
    """
    Definition of SGF property data type.
    """
    load: Callable
    dump: Callable
    default: Any = None
    validate: Optional[Callable] = None
    composition: Composition = Composition.SINGLE


@dataclass(frozen=True)
class Property:
    """
    Definition of SGF property.
    """
    ident: str
    name: str
    prop_type: PropType
    value_type: ValueTypeInfo


point_type = ValueTypeInfo(
    load = point_from_sgf,
    dump = sgf_from_point,
)

point_list_type = ValueTypeInfo(
    load = point_from_sgf,
    dump = sgf_from_point,
    composition = Composition.LIST,
)

color_type = ValueTypeInfo(
    load = color_from_sgf,
    dump = sgf_from_color,
)

size_type = ValueTypeInfo(
    load = size_from_sgf,
    dump = sgf_from_size,
)

number_type = ValueTypeInfo(
    load = lambda ctx, sgf: int(sgf),
    dump = lambda ctx, num: str(num),
)

real_type = ValueTypeInfo(
    load = lambda ctx, sgf: float(sgf),
    dump = lambda ctx, num: str(num),
)

# fixme
simple_text_type = ValueTypeInfo(
    load = text_from_sgf,
    dump = sgf_from_text,
    default = "",
)

text_type = ValueTypeInfo(
    load = text_from_sgf,
    dump = sgf_from_text,
    default = "",
)

editor_type = ValueTypeInfo(
    load = editor_from_sgf,
    dump = sgf_from_editor,
)

class PropertyRegistry:
    """
    Container class for all property definitions.
    """

    def __init__(self, *properties):
        self._properties = {}
        self.register(*properties)

    def __getattr__(self, ident):
        try:
            return self._properties[ident]
        except KeyError as err:
            raise AttributeError from err

    def register(self, *properties: Property):
        self._properties.update({
            prop.ident: prop for prop in properties
        })

    def get_property(self, ident: str):
        if ident not in self._properties:
            self.register(
                Property(ident, "Unknown Property", PropType.NONE, text_type)
            )
        return self._properties[ident]


PROPS = PropertyRegistry(
    # Move Properties
    Property("B", "Black Move", PropType.MOVE, point_type),
    Property("W", "White Move", PropType.MOVE, point_type),
    # Setup Properties
    Property("AB", "Add Black Stones", PropType.SETUP, point_list_type),
    Property("AW", "Add White Stones", PropType.SETUP, point_list_type),
    Property("AE", "Remove Stones", PropType.SETUP, point_list_type),
    Property("PL", "Set Player", PropType.SETUP, color_type),
    # Root Properties
    Property("SZ", "Board Size", PropType.ROOT, size_type),
    Property("FF", "File Format", PropType.ROOT, number_type),
    Property("GM", "Game", PropType.ROOT, number_type),
    Property("AP", "Application", PropType.ROOT, editor_type),
    Property("CA", "Charset", PropType.ROOT, simple_text_type),
    # Game Info Properties
    Property("HA", "Handicap", PropType.GAME_INFO, number_type),
    Property("KM", "Komi", PropType.GAME_INFO, real_type),
    Property("DT", "Date", PropType.GAME_INFO, simple_text_type),
    Property("EV", "Event", PropType.GAME_INFO, simple_text_type),
    Property("RO", "Round", PropType.GAME_INFO, simple_text_type),
    Property("PC", "Place", PropType.GAME_INFO, simple_text_type),
    Property("RU", "Rules", PropType.GAME_INFO, simple_text_type),
    Property("PC", "Place", PropType.GAME_INFO, simple_text_type),
    Property("RE", "Result", PropType.GAME_INFO, simple_text_type),
    Property("GN", "Game Name", PropType.GAME_INFO, simple_text_type),
    Property("PB", "Black's Name", PropType.GAME_INFO, simple_text_type),
    Property("PW", "White's Name", PropType.GAME_INFO, simple_text_type),
    Property("BR", "Black's Rank", PropType.GAME_INFO, simple_text_type),
    Property("WR", "White's Rank", PropType.GAME_INFO, simple_text_type),
    Property("BT", "Black's Team", PropType.GAME_INFO, simple_text_type),
    Property("WT", "White's Team", PropType.GAME_INFO, simple_text_type),
    Property("AN", "Annotator", PropType.GAME_INFO, simple_text_type),
    Property("CP", "Copyrights", PropType.GAME_INFO, simple_text_type),
    Property("GC", "Game Note", PropType.GAME_INFO, simple_text_type),
    Property("ON", "Opening Note", PropType.GAME_INFO, simple_text_type),
    Property("OT", "Overtime", PropType.GAME_INFO, simple_text_type),
    Property("SO", "Record Source", PropType.GAME_INFO, simple_text_type),
    Property("TM", "Main Time", PropType.GAME_INFO, simple_text_type),
    Property("US", "User", PropType.GAME_INFO, simple_text_type),
)
