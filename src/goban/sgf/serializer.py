# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from itertools import chain
from typing import Any

from goban.sgf.converters import Context
from goban.sgf.game_tree import SgfCollection, SgfNode
from goban.sgf.properties import PROPS, Composition, Property


def serialize_sgf(collection: SgfCollection) -> str:
    return "".join(
        _serialize_game_tree(
            Context(
                board_size = game_tree.root_node.properties[PROPS.SZ],
                encoding = game_tree.root_node.properties.get(PROPS.CA),
            ),
            game_tree.root_node,
        )
        for game_tree in collection.game_trees
    )


def _serialize_game_tree(context: Context, head: SgfNode) -> str:
    sequence = [head]

    while len(head.children) == 1:
        head = head.children[0]
        sequence.append(head)

    tree = "".join(
        chain(
            (_serialize_node(context, node) for node in sequence),
            (_serialize_game_tree(context, child) for child in head.children),
        )
    )

    return f"({tree})\n"


def _serialize_node(context: Context, node: SgfNode) -> str:
    props = "".join(
        _serialize_property(context, property, value)
        for property, value in node.properties.items()
        if value is not None
    )
    return f";{props}\n"


def _serialize_property(
    context: Context,
    property: Property,
    value: Any
) -> str:
    if property.value_type.composition is Composition.SINGLE:
        return f"{property.ident}[{property.value_type.dump(context, value)}]"

    if value:
        values = "".join(
            f"[{property.value_type.dump(context, item)}]" for item in value
        )
        return f"{property.ident}{values}"
    return f"{property.ident}"
