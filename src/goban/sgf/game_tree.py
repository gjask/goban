# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Any, Optional, Self
from weakref import ref

from goban.game.model import Color, Move, Point
from goban.sgf.properties import PROPS, Property


class SgfNode:
    _parent: ref[Self] | None
    children: list[Self]
    properties: dict[Property, Any]

    def __init__(self, parent, properties=None):
        self.parent = parent
        self.children = []
        self.properties = properties or {}

    @property
    def parent(self) -> Self | None:
        if self._parent is not None:
            unref = self._parent()

            if unref is None:
                raise RuntimeError("Parent was finalized.")

            return unref

    @parent.setter
    def parent(self, value: Self | None):
        if value is None:
            self._parent = None
        else:
            self._parent = ref(value)

    @property
    def is_root(self) -> bool:
        return not bool(self.parent)

    def get_root(self) -> Self:
        node = self
        while node.parent is not None:
            node = node.parent
        return node

    def set_property(self, prop: Property, value: Any):
        if prop.value_type.validate and not prop.value_type.validate(value):
            raise ValueError("Incompatible value %s for type %s", )
        self.properties[prop] = value

    # fixme
    def get_move(self) -> Move | None:
        for color, prop_def in (
            (Color.BLACK, PROPS.B),
            (Color.WHITE, PROPS.W),
        ):
            value = self.properties.get(prop_def)
            if value is not None:
                return Move(color, value)

    # fixme
    def set_move(self, move: Move):
        if move.color is Color.BLACK:
            self.set_property(PROPS.B, move.point)
        else:
            self.set_property(PROPS.W, move.point)

    def new_child(self, index: Optional[int] = None) -> Self:
        child = self.__class__(self)

        if index is None:
            self.children.append(child)
        else:
            self.children.insert(index, child)

        return child

    def set_index(self, index: int):
        if self.parent is None:
            raise ValueError("Can not change index for root node.")

        self.parent.children.remove(self)
        self.parent.children.insert(index, self)

    def remove(self):
        if self.parent is None:
            raise ValueError("Can not remove root node.")

        self.parent.children.remove(self)

    def get_sequence_above(self, stop_at: Optional[Self] = None) -> list[Self]:
        sequence = []
        node = self

        while node is not None and node is not stop_at:
            sequence.append(node)
            node = node.parent

        return sequence[::-1]

    def get_sequence_below(self, length: int = -1) -> list[Self]:
        sequence = []
        node = self

        while node.children and length != 0:
            node = node.children[0]
            sequence.append(node)
            length -= 1

        return sequence

    def find_move_above(self, point: Point) -> Self | None:
        node = self

        while node:
            move = node.get_move()

            if move is not None and move.point == point:
                return node

            node = node.parent

    def find_move_below(self, point: Point) -> Self | None:
        node = self

        while node.children:
            node = node.children[0]
            move = node.get_move()

            if move is not None and move.point == point:
                return node


class SgfGameTree:
    root_node: SgfNode

    def __init__(self, root_node: SgfNode):
        # todo implement game-info node search and validation
        self.root_node = root_node


class SgfCollection:
    game_trees: list[SgfGameTree]

    def __init__(self, game_trees: list[SgfGameTree]):
        self.game_trees = game_trees

    def get_tree_by_root_node(self, root: SgfNode) -> SgfGameTree:
        try:
            return next(
                tree for tree in self.game_trees if tree.root_node is root
            )

        except StopIteration:
            raise ValueError("Root node not found in collection.") from None
