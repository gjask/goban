# SPDX-FileCopyrightText: 2024 Jan Škrle <j.skrle@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Optional, Self

from gi.repository import Gio, GObject

from goban.game.board import Board
from goban.game.hoshi import default_handicap
from goban.game.model import Color, Move, Point
from goban.property_handlers import HANDLERS
from goban.property_proxy import GameInfo, RootInfo
from goban.sgf.game_tree import SgfCollection, SgfGameTree, SgfNode
from goban.sgf.properties import PROPS

FILE_FORMAT_VERSION = 4
GO_GAME_CODE = 1


class GameState(GObject.Object):
    __gtype_name__ = "GameState"

    collection: SgfCollection

    _board: Board
    _current_tree: SgfGameTree
    _current_node: SgfNode
    _root_info: RootInfo
    _game_info: GameInfo

    updated = GObject.Signal()
    # maybe set explicit flag?
    modified: bool = GObject.Property(type=bool, default=False)
    file: Optional[Gio.File] = GObject.Property(type=Gio.File)

    @GObject.Property(type=GObject.Object)
    def root_info(self) -> RootInfo:
        return self._root_info

    @root_info.setter
    def root_info(self, root_info: RootInfo):
        self._root_info = root_info
        root_info.connect("updated", lambda _: setattr(self, "modified", True))

    @GObject.Property(type=GObject.Object)
    def game_info(self) -> GameInfo:
        return self._game_info

    @game_info.setter
    def game_info(self, game_info: GameInfo):
        self._game_info = game_info
        game_info.connect("updated", lambda _: setattr(self, "modified", True))

    @property
    def game_tree(self) -> SgfGameTree:
        return self._current_tree

    @property
    def board(self) -> Board:
        return self._board

    @property
    def is_empty(self) -> bool:
        return not (self.file or self.modified)

    @property
    def is_at_first(self) -> bool:
        return self._current_node.is_root

    @property
    def is_at_last(self) -> bool:
        return not self._current_node.children

    # fixme (refactor away/elsewhere)
    @classmethod
    def new(cls, board_size: int, handicap: int, komi: float) -> Self:
        root_node = SgfNode(None)

        root_info = RootInfo(
            node = root_node,
            file_format = FILE_FORMAT_VERSION,
            game_ident = GO_GAME_CODE,
            board_size = board_size,
        )

        game_info = GameInfo(
            node = root_node,
            komi = komi,
        )

        if handicap >= 2:
            game_info.handicap = handicap

            # fixme
            root_node.set_property(
                PROPS.AB,
                default_handicap(board_size, handicap),
            )
            root_node.set_property(
                PROPS.PL,
                Color.WHITE,
            )

        return cls(
            collection = SgfCollection([SgfGameTree(root_node)]),
            root_info = root_info,
            game_info = game_info,
        )

    def __init__(
        self,
        collection: SgfCollection,
        root_info: Optional[RootInfo] = None,
        game_info: Optional[GameInfo] = None,
    ):
        super().__init__()
        self.collection = collection
        self._current_tree = collection.game_trees[0]
        self._current_node = self._current_tree.root_node

        self.root_info = root_info or RootInfo(self._current_tree.root_node)
        self.game_info = game_info or GameInfo(self._current_tree.root_node)

        self._process_root(self._current_node)
        self._process_node(self._current_node)

    def play_next(self, point: Optional[Point]):
        self.play(
            Move(
                color=self.board.next_color,
                point=point,
            )
        )

    def play(self, move: Move):
        for child in self._current_node.children:
            if child.get_move() == move:
                node = child
                break

        else:
            node = self._current_node.new_child()
            node.set_move(move)
            self.modified = True

        self.move_current_node(node)

    def move_to_point(self, point: Point):
        if self.board[point]:
            node = self._current_node.find_move_above(point)
        else:
            node = self._current_node.find_move_below(point)

        if node:
            self.move_current_node(node)

    def move_to_start(self):
        self.move_current_node(self._current_tree.root_node)

    def move_to_end(self):
        sequence = self._current_node.get_sequence_below()

        if sequence:
            self.move_current_node(sequence[-1], sequence=sequence)

    def move_by(self, offset: int):
        if offset >= 0:
            sequence = self._current_node.get_sequence_below(offset)
        else:
            sequence = self._current_node.get_sequence_above()[:offset]

        if sequence:
            self.move_current_node(sequence[-1], sequence)

    def move_current_tree(self, game_tree: SgfGameTree):
        self.move_current_node(game_tree.root_node)

    def move_current_node(self, target_node, sequence=None):
        if sequence is None:
            sequence = target_node.get_sequence_above(self._current_node)

        if sequence:
            if sequence[0].is_root:
                self._process_root(sequence[0])

            for node in sequence:
                self._process_node(node)

            self._current_node = target_node
            self.emit("updated")

    def _process_root(self, root: SgfNode):
        if root is not self._current_tree.root_node:
            new_tree = self.collection.get_tree_by_root_node(root)

            self._current_tree = new_tree
            self.root_info = RootInfo(new_tree.root_node)
            self.game_info = GameInfo(new_tree.root_node)

        self._reset_state()

    def _process_node(self, node: SgfNode):
        HANDLERS.process(self, node.properties)

    def _reset_state(self):
        self._board = Board(self.root_info.board_size)
