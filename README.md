# Goban

:warning: **Still heavily under development** :construction:

A Go/Weiqi/Baduk board and SGF editor for Gnome ecosystem.

![Screenshot](docs/images/goban.png)

## Contributing

Feel free to pickup on existing issue or create new issue for new features.
